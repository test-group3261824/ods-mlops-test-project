def hello(name: str) -> str:
    return f"Hello, {name}!"


def get_name() -> str:
    name = """
Hi, my name is, what? My name is, who?
My name is, chka-chka, Slim Shady
Hi, my name is, huh? My name is, what?
My name is, chka-chka, Slim Shady
"""
    return name


def do_a_barrel_roll() -> str:
    return "Sorry, I am just a python code. I can not do a barrel roll 😥"
